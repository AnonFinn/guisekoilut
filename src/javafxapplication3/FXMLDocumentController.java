/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication3;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextArea;
import java.io.*;

/**
 *
 * @author Henkka
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label, textlabel;
    
    @FXML private TextArea textarea;
    
    @FXML
    private TextField textfield, saveFilename, loadFilename;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("Hello World!");
        label.setText("Hello World!");
    }
    
    @FXML
    private void saveButtonAction(ActionEvent event)
    {
        textlabel.setText(textfield.getText());
    }
    
    @FXML
    private void handleTypingAction()
    {
        textlabel.setText(textfield.getText());
    }
    
    @FXML
    private void handleFileSaveButtonAction() throws FileNotFoundException, IOException
    {
        BufferedWriter out = new BufferedWriter(new FileWriter(saveFilename.getText()));
        out.write(textarea.getText());
        out.close();
    }
    
    @FXML
    private void handleFileLoadButtonAction() throws FileNotFoundException, IOException
    {
        String all = "";
        BufferedReader in = new BufferedReader(new FileReader(loadFilename.getText()));
        String s = in.readLine();
        
        while(s != null)
		{
                    all = all + s +"\n";
			
                    s = in.readLine();
		}
        
        in.close();
        
        
        textarea.setText(all);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
